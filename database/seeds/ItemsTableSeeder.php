<?php
declare(strict_types=1);

use Illuminate\Database\Seeder;

class ItemsTableSeeder extends Seeder
{
    protected $jsonFile = 'items.json';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = json_decode($this->getJson(), true);

        \DB::table('items')
            ->insert($json);
    }

    /**
     * @return resource
     *
     * @throws Exception
     */
    private function getJson()
    {
        $filename = dirname(__FILE__) . '/data/' . $this->jsonFile;

        $data = file_get_contents($filename);

        return $data;
    }
}
