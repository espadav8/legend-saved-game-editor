<!DOCTYPE html>
<html lang="en" class="@yield('html-class')" @yield('html_attr')>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="/favicon.ico">

        {{-- CSS --}}
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.2.3/foundation-flex.min.css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css?family=Ubuntu+Mono:400,700" rel="stylesheet">

        <style>
            html, body {
                font-size: 22px;
                font-family: 'Ubuntu Mono', monospace;
            }

            #contents {
                font-family: monospace;
                white-space: pre;
            }
        </style>
        <script
            src="https://code.jquery.com/jquery-3.1.0.min.js"
            integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s="
            crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    </head>

    <body>
        @foreach ($characters as $character)
            @include(
                'includes.character-sheet',
                [
                    'items' => $items,
                    'character' => $character,
                ]
            )
        @endforeach

        <div class="row column">
            Hex
            <div class="callout secondary">
                <pre class="hex-display"></pre>
            </div>
        </div>

        <form>
            <div>
                <label>
                    <span>File:</span>
                    <input type="file" id="fileInput">
                </label>
            </div>
            <div>
                <input id="btnRead" type="button" value="Read File">
                <button type="button" class="download">Download save</button>
            </div>
        </form>

            <div class="callout secondary">
                <div id="contents" class="hex-display">
            </div>
        </div>

        <script>
            let characterDataSize = 256; // 0x80
            let fileHeader = '1B2C3D4F';
            let fileData = '';

            let $hexDisplay = $('.hex-display');
            let $characterSheets = $('.character-sheet');
            let $items = $('.item');
            let $statInputs = $('input.stat');
            let buffer;
            let bytes;

            (function ($, w, undefined) {
                $(function () {
                    function padByte(byte) {
                        return byte.toString(16).padStart(2, "0");
                    }

                    function updateCharacterDataByte($characterSheet, location, newValue) {
                        const characterOffset = parseInt($characterSheet.data('offset'), 10)
                        console.log({characterOffset, location, newValue});

                        bytes[characterOffset + location] = newValue;
                    }

                    function updateItemLocation($item) {
                        const $characterSheet = $item.parents('.character-sheet:first');
                        const hexCode = $item.find('option:selected').data('hex');
                        const value = parseInt(hexCode, 16);
                        const itemOffset = parseInt($item.attr('data-offset'), 10);

                        updateCharacterDataByte($characterSheet, itemOffset, value);

                        buildHex();
                    }

                    function updateCharacterStat($stat) {
                        const $characterSheet = $stat.parents('.character-sheet:first');
                        const statOffset = parseInt($stat.attr('data-offset'), 16);
                        const value = parseInt($stat.val(), 10);

                        updateCharacterDataByte($characterSheet, statOffset, value);

                        buildHex();
                    }

                    function linePrefix(offset) {
                        return '0x' + offset.toString(16).padStart(4, "0") + ':'
                    }

                    function buildHex() {
                        // Show it as hex text
                        let offset = 0;
                        const lines = [];
                        let line = [linePrefix(offset)];
                        bytes.forEach((byte, index) => {
                            const hex = byte.toString(16).padStart(2, "0");
                            line.push(hex);
                            if (index % 16 === 15) {
                                lines.push(line.join(" "));
                                offset += 0x10;
                                line = [linePrefix(offset)];
                            } else if (index % 8 === 7) {
                                line.push(" ");
                            }
                        });
                        contentsDiv.text(lines.join("\n"));
                    }

                    function setValues() {
                        const inputs = $('input[data-offset]');
                        const selects = $('select[data-offset]');

                        inputs.each((_, input) => {
                            const characterOffset = parseInt($(input).parents('.character-sheet').first().data('offset'), 10)
                            const offset = parseInt($(input).data('offset'), 16);
                            $(input).val(bytes[characterOffset + offset]);
                        })

                        selects.each((_, select) => {
                            const characterOffset = parseInt($(select).parents('.character-sheet').first().data('offset'), 10)
                            const offset = parseInt($(select).data('offset'), 10);
                            $(select).val(bytes[characterOffset + offset]);
                        })

                        $('select').select2();
                    }

                    $items.on('change', function(e) {
                        updateItemLocation($(this));
                    });

                    $statInputs.on('change', function(e) {
                        updateCharacterStat($(this));
                    });

                    const fileInput = $("#fileInput");
                    const btnRead = $("#btnRead");
                    const contentsDiv = $("#contents");

                    const updateButton = () => {
                        btnRead.disabled = fileInput.prop('files').length === 0;
                    };

                    const readBinaryFile = async (file) => {
                        // Read into an array buffer, create
                        buffer = await file.arrayBuffer();
                        // Get a byte array for that buffer
                        bytes = new Uint8Array(buffer);

                        buildHex();
                        setValues()
                    };

                    fileInput.on("input", updateButton);
                    updateButton();

                    btnRead.on("click", () => {
                        const file = fileInput.prop('files')[0];
                        if (!file) {
                            return;
                        }
                        readBinaryFile(fileInput.prop('files')[0])
                            .catch(error => {
                                console.error(`Error reading file:`, error);
                            });
                    });

                    $('.download').on("click", () => {
                        var blobUrl = URL.createObjectURL(new Blob([bytes.buffer], {type: 'application/octet-stream'}));
                        window.open(blobUrl);
                    })
                });
            })(jQuery, window);
        </script>
    </body>
</html>
