<h1>Items</h1>

<ul>
    @foreach ($items as $item)
        <li>
            <strong>{{ $item->name }}</strong>: {{ $item->hex_code }}
        </li>
    @endforeach
</ul>
