<div class="row">
    <div class="column large-6">
        <div class="row">
            @include('includes.item-slot', ['items' => $items, 'offset' => $rowOffset + 0x26])
            @include('includes.item-slot', ['items' => $items, 'offset' => $rowOffset + 0x27])
            @include('includes.item-slot', ['items' => $items, 'offset' => $rowOffset + 0x28])
            @include('includes.item-slot', ['items' => $items, 'offset' => $rowOffset + 0x29])
        </div>
    </div>
    <div class="column large-6">
        <div class="row">
            @include('includes.item-slot', ['items' => $items, 'offset' => $rowOffset + 0x2A])
            @include('includes.item-slot', ['items' => $items, 'offset' => $rowOffset + 0x2B])
            @include('includes.item-slot', ['items' => $items, 'offset' => $rowOffset + 0x2C])
            @include('includes.item-slot', ['items' => $items, 'offset' => $rowOffset + 0x2D])
        </div>
    </div>
</div>
