<h2>{{ $character['name'] }}</h2>

<div class="character-sheet" data-offset="{{ $character['offset'] }}">
    @include('includes.character-stats')
    @include('includes.item-row', ['items' => $items, 'rowOffset' => 0x00])
    @include('includes.item-row', ['items' => $items, 'rowOffset' => 0x08])
</div>
