<select class="item" data-offset="{{ $offset }}">
    @foreach($items as $item)
        <option value="{{ hexdec($item->hex_code) }}" data-hex="{{ $item->hex_code }}">{{ $item->name }}</option>
    @endforeach
</select>
