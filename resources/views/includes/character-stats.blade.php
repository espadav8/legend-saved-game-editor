<div class="row">
    <div class="column large-4">
        <div class="input-group">
            <label class="input-group-label" for="{{ $character['id'] }}-af">Af:</label>
            <input type="number" value="0"
                   name="{{ $character['id'] }}-af"
                   id="{{ $character['id'] }}-af"
                   class="input-group-field" readonly disabled />
        </div>
    </div>
    <div class="column large-4">
        <div class="input-group">
            <label class="input-group-label" for="{{ $character['id'] }}-df">Df:</label>
            <input type="number" value="0"
                   name="{{ $character['id'] }}-df"
                   id="{{ $character['id'] }}-df"
                   class="input-group-field" readonly disabled />
        </div>
    </div>
    <div class="column large-4">
        <div class="input-group">
            <label class="input-group-label" for="{{ $character['id'] }}-ac">Ac:</label>
            <input type="number" value="0" step="1" min="-245" max="11"
                   name="{{ $character['id'] }}-ac"
                   id="{{ $character['id'] }}-ac"
                   data-offset="10"
                   class="input-group-field stat" />
        </div>
    </div>
</div>
<div class="row">
    <div class="column large-4">
        <div class="input-group">
            <label class="input-group-label" for="{{ $character['id'] }}-str">Str</label>
            <input type="number" value="0" step="1" min="0" max="255"
                   name="{{ $character['id'] }}-str"
                   id="{{ $character['id'] }}-str"
                   data-offset="12"
                   class="input-group-field stat" />
        </div>
    </div>
    <div class="column large-4">
        <div class="input-group">
            <label class="input-group-label" for="{{ $character['id'] }}-int">Int</label>
            <input type="number" value="0" step="1" min="0" max="255"
                   name="{{ $character['id'] }}-int"
                   id="{{ $character['id'] }}-int"
                   data-offset="14"
                   class="input-group-field stat" />
        </div>
    </div>
    <div class="column large-4">
        <div class="input-group">
            <label class="input-group-label" for="{{ $character['id'] }}-spd">Spd</label>
            <input type="number" value="0" step="1" min="0" max="255"
                   name="{{ $character['id'] }}-spd"
                   id="{{ $character['id'] }}-spd"
                   data-offset="16"
                   class="input-group-field stat" />
        </div>
    </div>
</div>
<div class="row">
    <div class="column large-4">
        <div class="input-group">
            <label class="input-group-label" for="{{ $character['id'] }}-dex">Dex</label>
            <input type="number" value="0" step="1" min="0" max="255"
                   name="{{ $character['id'] }}-dex"
                   id="{{ $character['id'] }}-dex"
                   data-offset="18"
                   class="input-group-field stat" />
        </div>
    </div>
    <div class="column large-4">
        <div class="input-group">
            <label class="input-group-label" for="{{ $character['id'] }}-con">Con</label>
            <input type="number" value="0" step="1" min="0" max="255"
                   name="{{ $character['id'] }}-con"
                   id="{{ $character['id'] }}-con"
                   data-offset="1A"
                   class="input-group-field stat" />
        </div>
    </div>
    <div class="column large-4">
        <div class="input-group">
            <label class="input-group-label" for="{{ $character['id'] }}-lck">Lck</label>
            <input type="number" value="0" step="1" min="0" max="255"
                   name="{{ $character['id'] }}-lck"
                   id="{{ $character['id'] }}-lck"
                   data-offset="1C"
                   class="input-group-field stat" />
        </div>
    </div>
</div>
