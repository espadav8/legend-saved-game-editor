<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Item;

class HomeController extends Controller
{
    public function index()
    {
        $items = Item::all();
        $characters = [
            [
                'name' => 'Berserker',
                'offset' => 0x04,
                'id' => 'berserker',
            ], [
                'name' => 'Troubador',
                'offset' => 0x84,
                'id' => 'troubador',
            ], [
                'name' => 'Assassin',
                'offset' => 0x104,
                'id' => 'assassin',
            ], [
                'name' => 'Runemaster',
                'offset' => 0x184,
                'id' => 'runemaster',
            ],
        ];

        return view(
            'home',
            [
                'items' => $items,
                'characters' => $characters,
            ]
        );
    }
}
