<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;

use App\Http\Requests;

class ItemController extends Controller
{
    /**
     *
     */
    public function index()
    {
        $items = Item::all();

        return view(
            'items.index',
            [
                'items' => $items,
            ]
        );
    }
}
